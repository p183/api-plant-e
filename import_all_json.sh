#!/bin/sh

ls -1 /import/json/*.json | sed 's/.json$//' | while read col; do 
    echo $col
    docker exec db mongoimport -u plante -p "plante" -d plante -c plant "$col".json; 
done
