const express = require('express');
const dotenv = require('dotenv-safe');
const mongoose = require('mongoose');
const cors = require('cors')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

dotenv.config();

mongoose.connect(
    `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => {
    console.log('successfully connected to the database');
});

const app = express();

app.use(cors())
app.use(express.json());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, { }));

app.get('/', (req, res) => {
    return res.status(200).json({ status: 'success', data: { message: 'Hello World' } });
});


// All routes we'll use

const userRouter = require('./components/user/userAPI');
const plantsRouter = require('./components/plants');

app.use('/user', userRouter);
app.use('/plants', plantsRouter);

module.exports = app;