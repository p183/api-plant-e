const app = require('../app');
const http = require('http');

const normalizePort = (value) => {
    const port = parseInt(value, 10);

    if (Number.isNaN(port)) {
        return value;
    }
    if (port >= 0) {
        return port;
    }
    return false;
}

const port = normalizePort(process.env.APP_PORT || '3000');

app.set('port', port);

const server = http.createServer(app);

server.listen(port);
server.on('error', (err) => {
    if (err.syscall !== 'listen') {
        throw err;
    }

    const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

    switch (err.code) {
        case 'EACCESS':
            throw new Error(`${bind} requires elevated privileges`);
        case 'EADDRINUSE':
            throw new Error(`${bind} is already in use`);
        default:
            throw err;
    }
});
server.on('listening', () => {
    const addr = server.address();
    const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
    console.log(`app listening on ${bind}`);
});