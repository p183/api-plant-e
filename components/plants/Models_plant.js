const Joi = require('joi');
const mongoose = require('mongoose');


const Plant = new mongoose.Schema({
    pid: {
        type: String
    },
    basic: {
        floral_language: {
            type: String
        },
        origin: {
            type: String
        },
        production: {
            type: String
        },
        category: {
            type: String
        },
        blooming: {
            type: String
        },
        color: {
            type: String
        }
    },  
    display_pid: {
        type: String  
    },
    maintenance: {
        size: {
            type: String
        },
        soil: {
            type: String
        },
        sunlight: {
            type: String
        },
        watering: {
            type: String
        },
        fertilization: {
            type:  String
        },
        pruning: {
            type: String
        }
    },
    parameter: {
        max_light_mmol: {
            type: Number
        },
        min_light_mmol: {
            type: Number
        },
        max_light_lux: {
            type: Number
        },
        min_light_lux: {
            type: Number
        },
        max_temp: {
            type: Number
        },
        min_temp: {
            type: Number
        },
        max_env_humid: {
            type: Number
        },
        min_env_humid: {
            type: Number
        },
        max_soil_moist: {
            type: Number
        },
        min_soil_moist: {
            type: Number
        },
        max_soil_ec: {
            type: Number
        },
        min_soil_ec: {
            type: Number
        }
    },
    image: {
        type: String
    }
})

module.exports = mongoose.model("Plant", Plant);