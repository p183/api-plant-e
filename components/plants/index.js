const express = require('express');
const router = express.Router();
const { list, show, signin, create } = require('./controller');


router.get('/', list);
router.post('/', create);
router.get('/all', show);

module.exports = router;