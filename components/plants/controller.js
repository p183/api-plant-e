const Plant = require('./Models_plant');

const findPlant = async (data) => {
    console.log("in findplant", data.pid)
    const result = await Plant.find(
        { 
            pid: { $regex: data.pid, $options: "i" }
         }
    )
    if (result) {
        console.log("result of the find plant", result);
        return result;
    } else {
        return -1;
    }
}

const showall = async(skip, limit) => {
    const count = await Plant.count({});
    console.log(skip, limit)
    const result = await Plant.find({}).skip(skip).limit(limit);
    return {count, result};
}

const createPlante = async (data) => {
    console.log('in create plant', data.pid);
    const plant =  await Plant.findOne({pid: data.pid})
    if (plant) {
        return {"message": "plant exist"};
    } else {
        const newPlant = await Plant.create({
            pid: data.pid
        })
        return newPlant;
    }
}

const list = async (req, res) => {
    console.log(req.body);
    const result = await findPlant(req.body);
    return res.status(200).json(result);
};

const show = async (req, res) => {
    const result = await showall(parseInt(req.query.skip), parseInt(req.query.limit));
    return res.status(200).json(result);
};

const create = async (req, res) => {
const response = await createPlante(req.body);
console.log(response);
return res.status(200).json(response);

}

module.exports = {
    list,
    show,
    create,
}