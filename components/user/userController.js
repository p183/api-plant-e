const UserDAL = require ('./userDAL');
const UserService = require('./userService');

const getUsers = async (req, res) => {
    try {
        const DAL = new UserDAL();
        const service = new UserService(DAL);
        const result = await service.getUsers();

        return res.status(200).json({users: result}); 
    } catch (err) {
        throw err;
    }
}

const signup = async (req, res) => {
    try {
        const DTO = req.body;
        const DAL = new UserDAL();
        const service = new UserService(DAL);
        const result = await service.signup(DTO);

        return res.status(200).json({user: result}); 
    } catch (err) {
        throw err;
    }
}

const signin = async (req, res) => {
    try {
        const DTO = req.body;
        const DAL = new UserDAL();
        const service = new UserService(DAL);
        const result = await service.signin(DTO);

        return res.status(200).json({token: result}); 
    } catch (err) {
        throw err;
    }
}

module.exports = {
    getUsers,
    signup,
    signin
}