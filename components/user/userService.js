const UserDAL = require('./userDAL');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');

class UserService {
    constructor(DAL) {
        this.DAL = DAL;
    }

    async getUsers() {
        try {
            const users = await this.DAL.getUsers();

            return users;
        } catch (err) {
            throw err;
        }
    }

    async signup(DTO) {
        try {
            const { email, firstname, lastname, password } = DTO;
            const hashedPassword = passwordHash.generate(password);

            DTO = { email, firstname, lastname, hashedPassword };
            
            const user = await this.DAL.createUser(DTO);

            return user;
        } catch (err) {
            throw err;
        }
    }

    async signin(DTO) {
        try {
            const { email, password } = DTO;

            const user = await this.DAL.getUserByEmail(email);

            if (user === null) {
                throw new Error('Wrong email');
            }

            console.log(user);
            const validPassword = passwordHash.verify(password, user.password);

            if (!validPassword) {
                throw new Error('Wrong password');
            }

            const token = jwt.sign(
                { id: user._id },
                process.env.JWT_SECRET,
                {
                    expiresIn: '1d'
                }
            );

            return token;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = UserService;