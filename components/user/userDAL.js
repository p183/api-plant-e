const User = require('./userSchema');

class UserDal {
    async getUsers() {
        const users = User.find({});

        return users;
    }

    async createUser(DTO) {
        const { email, firstname, lastname, hashedPassword } = DTO;

        const user = await User.create({
            email,
            firstname,
            lastname,
            password: hashedPassword
        });

        return user;
    }

    async getUserByEmail(email) {
        const user = await User.findOne({
            email
        });

        return user;
    }
}

module.exports = UserDal;