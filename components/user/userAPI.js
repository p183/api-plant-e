const express = require('express');
const router = express.Router();
const { getUsers, signup, signin } = require('./userController');

router.get('/all', getUsers);
router.post('/signup', signup);
router.post('/signin', signin);

module.exports = router;